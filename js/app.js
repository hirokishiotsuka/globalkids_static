var slide = [];

function windowResize() {
  var margin = ($(window).width() - 330) / 2;
  $('#carouesel_list').css('left', margin + 'px');
}

function clickLabel(num) {
  slide[num].checked = true;
  slide[num+5].checked = true;
}

setInterval(function () {
  var client = [];

  for (var i = 0; i < 10; i++) {
    if (slide[i].checked) {
      client.push(slide[i]);
    }
  }

  for (var i = 0; i < client.length; i++) {
    var num = Number(client[i].id.charAt(9));

    if (num === 5) {
      slide[0].checked = true;
      slide[5].checked = true;
    } else {
      slide[num].checked = true;
      slide[num+5].checked = true;
    }
  }
}, 5000);

function nextForm(num) {
  var list = $('#step' + num);
  var ul = $('#form_step_list');
  var form = $('#form');

  for (var i = 0; i < ul.children().length; i++) {
    if (ul.children(i).hasClass('on')) {
      ul.children(i).removeClass('on');
    }
  }
  list.addClass('on');

  form.animate({'right': (100 * (num - 1)) + '%'}, 500);
}

function init() {

  $(window).resize(windowResize());

  for (var i = 1; i < 6; i++) {
    slide[i-1] = document.getElementById('carousel_' + i +'_ctl_pc');
  }

  for (var i = 1; i < 6; i++) {
    slide[i+4] = document.getElementById('carousel_' + i +'_ctl_sp');
  }
}

init();
